package Utils;

public class Constants {
	public String NULL = "";
	public String SELECT = "select";
	public String DOT = "dot";
	public String RECTANGLE = "rectangle";
	public String LINE = "line";
	public String DOTTEDLINE = "dottedLine";
	public String DASHEDLINE = "dashedLine";
	public String ELLIPSE = "ellipse";
	public String POLYGON = "polygon";
	public String POLYLINE = "polyline";
	public String FILLBUCKET = "fillBucket";
	public String LINECOLOR = "lineColor";

}
