package Utils;

import java.io.Serializable;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;

public class AllProperties implements Serializable {

	// Global
	private String fillColor, strokeColor;
	private String shapeName, nickName;
	private StrokeLineCap strokeLineCap;
	private ArrayList<Double> strokeDashArray;

	// Rectangle
	private double getX, getY, width, height, rotate, scaleX, scaleY, lineWidth;

	// Line
	private double startX, startY, endX, endY;

	// Ellipse
	private double centerX, centerY, radiusX, radiusY;
	// Dot
	private double radius;

	// Polygon / Polyline
	private ArrayList<Double> polyPointsArrayList;
	
	// Group
	private boolean isGroup = false;
	private int groupSize = 0;
	
	private static final long serialVersionUID = 1L;

	public AllProperties() {
		fillColor = null;
		strokeColor = "0xffffffff";
		shapeName = null;
		nickName = null;
		strokeLineCap = null;
		strokeDashArray = null;
		getX = 0;
		getY = 0;
		width = 0;
		height = 0;
		rotate = 0;
		scaleX = 0;
		scaleY = 0;
		lineWidth = 0;
		centerX = 0;
		centerY = 0;
		radiusX = 0;
		radiusY = 0;
		startX = 0;
		startY = 0;
		endX = 0;
		endY = 0;
		polyPointsArrayList = new ArrayList<>();
	}

	public void printAll() {
		System.out.println("Shape name: " + shapeName);
		System.out.println("X: " + getX);
		System.out.println("Y: " + getY);
		System.out.println("Width: " + width);
		System.out.println("Height: " + height);
		System.out.println("FillColor: " + fillColor);
		System.out.println("StrokeColor: " + strokeColor);
		System.out.println("LineWidth: " + lineWidth);
		System.out.println("Rotate: " + rotate);
		System.out.println("ScaleX: " + scaleX);
		System.out.println("ScaleY: " + scaleY);
	}

	public ArrayList<Double> getPolyPointsArrayList() {
		return polyPointsArrayList;
	}

	public void setPolyPointsArrayList(ObservableList<Double> observableList) {
		polyPointsArrayList.clear();
		for(int i = 0; i < observableList.size(); i++){
			polyPointsArrayList.add(observableList.get(i));
		}
	}

	public double getCenterX() {
		return centerX;
	}

	public void setCenterX(double cetnerX) {
		this.centerX = cetnerX;
	}

	public double getCenterY() {
		return centerY;
	}

	public void setCenterY(double centerY) {
		this.centerY = centerY;
	}

	public double getRadiusX() {
		return radiusX;
	}

	public void setRadiusX(double radiusX) {
		this.radiusX = radiusX;
	}

	public double getRadiusY() {
		return radiusY;
	}

	public void setRadiusY(double radiusY) {
		this.radiusY = radiusY;
	}

	public StrokeLineCap getStrokeLineCap() {
		return strokeLineCap;
	}

	public void setStrokeLineCap(StrokeLineCap strokeLineCap) {
		this.strokeLineCap = strokeLineCap;
	}

	public ArrayList<Double> getStrokeDashArray() {
		return strokeDashArray;
	}

	public void setStrokeDashArray(ArrayList<Double> observableList) {
		this.strokeDashArray = observableList;
	}

	public double getGetX() {
		return getX;
	}

	public void setGetX(double getX) {
		this.getX = getX;
	}

	public double getGetY() {
		return getY;
	}

	public void setGetY(double getY) {
		this.getY = getY;
	}

	public double getLineWidth() {
		return lineWidth;
	}

	public void setLineWidth(double lineWidth) {
		this.lineWidth = lineWidth;
	}

	public double getStartX() {
		return startX;
	}

	public void setStartX(double startX) {
		this.startX = startX;
	}

	public double getStartY() {
		return startY;
	}

	public void setStartY(double startY) {
		this.startY = startY;
	}

	public double getEndX() {
		return endX;
	}

	public void setEndX(double endX) {
		this.endX = endX;
	}

	public double getEndY() {
		return endY;
	}

	public void setEndY(double endY) {
		this.endY = endY;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public double getX() {
		return getX;
	}

	public void setX(double d) {
		getX = d;
	}

	public double getY() {
		return getY;
	}

	public void setY(double d) {
		getY = d;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getWidth() {
		return width;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getHeight() {
		return height;
	}

	public String getFillColor() {
		return fillColor;
	}

	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	public String getStrokeColor() {
		return strokeColor;
	}

	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}

	public void setShapeName(String value) {
		shapeName = value;
		nickName = value;
	}

	public String getShapeName() {
		return shapeName;
	}

	public double getRotate() {
		return rotate;
	}

	public void setRotate(double rotate) {
		this.rotate = rotate;
	}

	public double getScaleX() {
		return scaleX;
	}

	public void setScaleX(double scaleX) {
		this.scaleX = scaleX;
	}

	public double getScaleY() {
		return scaleY;
	}

	public void setScaleY(double scaleY) {
		this.scaleY = scaleY;
	}

	public String toString() {
		return nickName;
	}

	public String getNickName() {
		return nickName;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public void setGroupSize(int size) {
		groupSize = size;
	}
	
	public int getGroupSize(){
		return groupSize;
	}
}
