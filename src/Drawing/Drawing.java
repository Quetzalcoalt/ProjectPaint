package Drawing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import Utils.AllProperties;
import Utils.Constants;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;

//remove items from hashmap 
//make a dialog for saving and opening a file
//paste Copied nodes
//maybe add an edit button to edit the shape start and end X and Y
//write Text 
//make a slider for opacity

public class Drawing extends Constants {

	private String toolName;
	private double startX;
	private double startY;
	private Node selectedShape;
	private AllProperties savingPropertiesObject;
	private Line line;
	private Rectangle rect;
	private Ellipse ellipse;
	private Circle dot;
	private Polyline polyline;
	private Polygon polygon;
	private Polyline triangle;
	private ArrayList<Double> polyPointsArrayList;
	private ArrayList<Double> trianglePointsArrayList;
	private double triX, triY;
	private Color color = Color.BLACK;
	private ArrayList<Node> selectedShapesList;
	private ArrayList<Node> copiedShapesList;
	private double lineWidth;
	private double tempX, tempY;
	private double rotation;
	private double oldMouseX;
	private double oldMouseY;
	private double oldTranslateX;
	private ArrayList<Double> translateXList;
	private ArrayList<Double> translateYList;
	private double oldTranslateY;
	private Group undoGroup;
	private Group redoGroup;

	private double[] selectionPointsArray;

	private boolean isControl;
	private LinkedHashMap<Node, AllProperties> nodeToAllPropertiesMap = new LinkedHashMap<Node, AllProperties>();
	// private Canvas canvas;

	public Drawing(GraphicsContext gc, Group group, Canvas canvas, TreeItem<AllProperties> rootItem, Scene scene,
			Label label, ToolBar toolbar) {

		// Init make canvas color white
		gc.setFill(Color.rgb(242, 242, 242));
		gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

		// Init variable
		toolName = NULL;
		startX = 0; // Saves the starting X coordinate every Shape
		startY = 0; // Saves the starting Y coordinate every Shape
		selectedShape = null;
		selectedShapesList = new ArrayList<Node>();
		copiedShapesList = new ArrayList<Node>();
		translateXList = new ArrayList<Double>();
		translateYList = new ArrayList<Double>();
		lineWidth = 1.0; // default lineWidth
		tempX = -1; // Uses tempX to remember the first coordinate for drawing
					// the line for the Polygon and Polyline
		tempY = -1; // Uses tempY to remember the first coordinate for drawing
					// the line for the Polygon and Polyline
		polyPointsArrayList = new ArrayList<Double>();
		trianglePointsArrayList = new ArrayList<Double>();
		triX = 0;
		triY = 0;
		rotation = 0; // Used to remember the shape current rotation and setting
						// new rotation to shape
		isControl = false;

		undoGroup = new Group();
		redoGroup = new Group();

		selectionPointsArray = new double[5];

		group.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_DRAGGED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						if (e.getButton() == MouseButton.PRIMARY) {
							if (toolName.equals(SELECT)) {
								clearCanvas(gc, canvas);
								moveShape(gc, e);
							}
							if (toolName.equals(DOT)) {
								clearCanvas(gc, canvas);
							}
							if (toolName.equals(RECTANGLE)) {
								clearCanvas(gc, canvas);
								drawRectangle(gc, e);
							}
							if (toolName.equals(LINE)) {
								clearCanvas(gc, canvas);
								drawLine(gc, e);
							}
							if (toolName.equals(DOTTEDLINE)) {
								clearCanvas(gc, canvas);
								drawDottedLine(gc, e);
							}
							if (toolName.equals(DASHEDLINE)) {
								clearCanvas(gc, canvas);
								drawDashedLine(gc, e);
							}
							if (toolName.equals(ELLIPSE)) {
								clearCanvas(gc, canvas);
								drawEllipse(gc, e);
							}
							if (toolName.equals(POLYGON)) {
							}
							if (toolName.equals(POLYLINE)) {
							}
							if(toolName.equals("Triangle")){
								clearCanvas(gc, canvas);
								drawTriangle(gc, e, triX, triY);
							}
						}
					}
				});

		group.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_PRESSED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						if (e.getButton() == MouseButton.PRIMARY) {
							groupMousePressed(gc, canvas, group, rootItem, e);
						}
						System.out.println(selectedShapesList.size());
						if (selectedShapesList.size() == 0) {
							for (Node node : toolbar.getItems()) {
								if (node.getAccessibleText() != null) {
									if (node.getAccessibleText().equals("rotateRight45")
											|| node.getAccessibleText().equals("rotateLeft45")
											|| node.getAccessibleText().equals(FILLBUCKET)
											|| node.getAccessibleText().equals(LINECOLOR)
											|| node.getAccessibleText().equals("dashSlider1")
											|| node.getAccessibleText().equals("dashSlider2")) {
										node.setVisible(false);
									}
								}
							}
						} else {
							if (isAShapeInTheList(selectedShapesList, LINE)) {
								for (Node node : toolbar.getItems()) {
									if (node.getAccessibleText() != null) {
										if (node.getAccessibleText().equals("rotateRight45")
												|| node.getAccessibleText().equals("rotateLeft45")
												|| node.getAccessibleText().equals(FILLBUCKET)
												|| node.getAccessibleText().equals(LINECOLOR)
												|| node.getAccessibleText().equals("dashSlider1")
												|| node.getAccessibleText().equals("dashSlider2")) {
											node.setVisible(true);
										}
									}
								}
							} else {
								for (Node node : toolbar.getItems()) {
									if (node.getAccessibleText() != null) {
										if (node.getAccessibleText().equals("rotateRight45")
												|| node.getAccessibleText().equals("rotateLeft45")
												|| node.getAccessibleText().equals(FILLBUCKET)
												|| node.getAccessibleText().equals(LINECOLOR)) {
											node.setVisible(true);
										}
										if (node.getAccessibleText().equals("dashSlider1")
												|| node.getAccessibleText().equals("dashSlider2")) {
											node.setVisible(false);
										}
									}
								}
							}
						}
					}
				});

		group.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_RELEASED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						if (e.getButton() == MouseButton.PRIMARY) {
							if (startX != e.getX() && startY != e.getY()) {
								groupMouseReleased(gc, canvas, group, rootItem);
							}
						}
						if (selectedShapesList.size() == translateXList.size()) {
							for (int i = 0; i < selectedShapesList.size(); i++) {
								translateXList.set(i, selectedShapesList.get(i).getTranslateX());
								translateYList.set(i, selectedShapesList.get(i).getTranslateY());
							}
						} else {
							System.out.println("Something is wrong with the Translates");
						}
					}
				});

		group.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_MOVED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						label.setText("X: " + e.getX() + " Y: " + e.getY());
					}
				});
	}

	private boolean isAShapeInTheList(ArrayList<Node> arr, String shape) {
		for (Node node : arr) {
			if (shape.equals(LINE)) {
				if (node instanceof Line) {
					return true;
				}
			} else if (shape.equals(RECTANGLE)) {
				if (node instanceof Rectangle) {
					return true;
				}
			} else if (shape.equals(ELLIPSE)) {
				if (node instanceof Ellipse) {
					return true;
				}
			} else if (shape.equals(POLYGON)) {
				if (node instanceof Polygon) {
					return true;
				}
			} else if (shape.equals(POLYLINE)) {
				if (node instanceof Polyline) {
					return true;
				}
			}
		}
		return false;
	}

	public void groupMousePressed(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> rootItem,
			MouseEvent e) {
		startX = e.getX();
		startY = e.getY();
		if (toolName.equals(DOT)) {
			selectedShapesList.clear();
			dot = new Circle();
			dot.setFill(color);
			dot.setStroke(color);
			dot.setCenterX(e.getX());
			dot.setCenterY(e.getY());
			dot.setRadius(lineWidth);
			group.getChildren().add(dot);
			createAndAddPropertyToHashMap(dot);
		}
		if (toolName.equals(LINE)) {
			selectedShapesList.clear();
			line = new Line();
		}
		if (toolName.equals(DOTTEDLINE)) {
			selectedShapesList.clear();
			line = new Line();
		}
		if (toolName.equals(DASHEDLINE)) {
			selectedShapesList.clear();
			line = new Line();
		}
		if (toolName.equals(RECTANGLE)) {
			selectedShapesList.clear();
			rect = new Rectangle();
		}
		if (toolName.equals(ELLIPSE)) {
			selectedShapesList.clear();
			ellipse = new Ellipse();
		}
		if (toolName.equals(POLYGON)) {
			selectedShapesList.clear();
			if (polygon instanceof Polygon) {
				polyPointsArrayList.add(startX);
				polyPointsArrayList.add(startY);
			} else {
				polygon = new Polygon();
				polyPointsArrayList.clear();
				polyPointsArrayList.add(startX);
				polyPointsArrayList.add(startY);
			}
			drawPolygonPolyline(gc, e, startX, startY);
		}
		if (toolName.equals(POLYLINE)) {
			selectedShapesList.clear();
			if (polyline instanceof Polyline) {
				polyPointsArrayList.add(startX);
				polyPointsArrayList.add(startY);
			} else {
				polyline = new Polyline();
				polyPointsArrayList.clear();
				polyPointsArrayList.add(startX);
				polyPointsArrayList.add(startY);
			}
			drawPolygonPolyline(gc, e, startX, startY);
		}

		if (toolName.equals(SELECT)) {
			selectedShape = null;
			boolean isSelected = false;
			for (int i = group.getChildren().size() - 1; i >= 0; i--) {
				if (!(group.getChildren().get(i) instanceof Canvas)) {
					Node node = group.getChildren().get(i);
					if (node.getBoundsInParent().contains(e.getX(), e.getY())) {
						isSelected = true;
						selectedShape = node;
						if (!isControl) {
							selectedShapesList.clear();
							translateXList.clear();
							translateYList.clear();
						}
						if (!selectedShapesList.contains(selectedShape)) {
							selectedShapesList.add(selectedShape);
							translateXList.add(selectedShape.getTranslateX());
							translateYList.add(selectedShape.getTranslateY());
						}
						clearCanvas(gc, canvas);
						drawSelection(gc, selectedShapesList);
						break;
					}
				}
			}
			if (!isSelected) {
				selectedShapesList.clear();
				translateXList.clear();
				translateYList.clear();
				clearCanvas(gc, canvas);
			}
			oldMouseX = e.getX();
			oldMouseY = e.getY();
		}

		if (toolName.equals(FILLBUCKET)) {
			if (selectedShapesList != null) {
				for (int i = 0; i < selectedShapesList.size(); i++) {
					if (selectedShapesList.get(i) instanceof Group) {
						for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
							if (node instanceof Group) {
								setFillColor(color, ((Group) node).getChildren());
							} else {
								((Shape) node).setFill(color);
							}
						}
					} else {
						((Shape) selectedShapesList.get(i)).setFill(color);
					}
				}
				clearCanvas(gc, canvas);
				drawSelection(gc, selectedShapesList);
			}
		}

		if (toolName.equals(LINECOLOR)) {
			if (selectedShapesList != null) {
				for (int i = 0; i < selectedShapesList.size(); i++) {
					if (selectedShapesList.get(i) instanceof Group) {
						for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
							if (node instanceof Group) {
								setStrokeColor(color, ((Group) node).getChildren());
							} else {
								((Shape) node).setStroke(color);
							}
						}
					} else {
						((Shape) selectedShapesList.get(i)).setStroke(color);
					}
				}
				clearCanvas(gc, canvas);
				drawSelection(gc, selectedShapesList);
			}
		}
		
		if(toolName.equals("Triangle")){
			selectedShapesList.clear();
			triangle = new Polyline();
			triX = e.getX();
			triY = e.getY();
		}
	}

	public void groupMouseReleased(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> rootItem) {
		if (toolName.equals(SELECT)) {
		}
		if (toolName.equals(DOT)) {
		}
		if (toolName.equals(LINE) || toolName.equals(DOTTEDLINE) || toolName.equals(DASHEDLINE)) {
			line.setStroke(color);
			line.setStrokeWidth(lineWidth);
			clearCanvas(gc, canvas);
			group.getChildren().add(line);
			createAndAddPropertyToHashMap(line);
		}
		if (toolName.equals(RECTANGLE)) {
			rect.setFill(Color.WHITE);
			rect.setStroke(color);
			rect.setStrokeWidth(lineWidth);
			clearCanvas(gc, canvas);
			group.getChildren().add(rect);
			createAndAddPropertyToHashMap(rect);
		}
		if (toolName.equals(ELLIPSE)) {
			ellipse.setFill(Color.WHITE);
			ellipse.setStroke(color);
			ellipse.setStrokeWidth(lineWidth);
			clearCanvas(gc, canvas);
			group.getChildren().add(ellipse);
			createAndAddPropertyToHashMap(ellipse);
		}
		if (toolName.equals(POLYGON)) {
		}

		if (toolName.equals(POLYLINE)) {
		}
		
		if(toolName.equals("Triangle")){
			group.getChildren().add(triangle);
			createAndAddPropertyToHashMap(triangle);
		}
		treeViewUpdate(group, rootItem);
	}

	public void moveShape(GraphicsContext gc, MouseEvent e) {
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				selectedShapesList.get(i).setTranslateX(translateXList.get(i) + e.getX() - oldMouseX);
				selectedShapesList.get(i).setTranslateY(translateYList.get(i) + e.getY() - oldMouseY);
			}
			drawSelection(gc, selectedShapesList);
		} else {
			selectionPointsArray = drawDragginSelection(gc, e);
		}
	}

	public void getNickName() {
		int i = 0;
		for (AllProperties p : nodeToAllPropertiesMap.values()) {
			{
				System.out.println("NickName [" + i + "]:" + p.toString());
				i++;
			}
		}
	}

	public void updateTreeViewVersion3(Group group, TreeItem<AllProperties> parent) {
		AllProperties groupProperty = nodeToAllPropertiesMap.get(group);
		TreeItem<AllProperties> groupTI = new TreeItem<AllProperties>(groupProperty);
		parent.getChildren().add(groupTI);
		parent.setExpanded(true);
		for (Node node : group.getChildren()) {
			if (node instanceof Shape) {
				AllProperties property = nodeToAllPropertiesMap.get(node);
				TreeItem<AllProperties> shape = new TreeItem<AllProperties>(property);
				groupTI.getChildren().add(shape);
			} else if (node instanceof Group) {
				updateTreeViewVersion3((Group) node, groupTI);
			}
		}
	}

	public void treeViewUpdate(Group group, TreeItem<AllProperties> rootTreeItem) {
		rootTreeItem.getChildren().clear();
		rootTreeItem.setExpanded(true);
		// savingArrayList.clear();
		// nodeToAllPropertiesMap.clear();
		// savingProperties(group);
		for (Node node : group.getChildren()) {
			if (!(node instanceof Canvas)) {
				if (node instanceof Shape) {
					AllProperties property = nodeToAllPropertiesMap.get(node);
					TreeItem<AllProperties> shape = new TreeItem<AllProperties>(property);
					rootTreeItem.getChildren().add(shape);
				} else if (node instanceof Group) {
					updateTreeViewVersion3((Group) node, rootTreeItem);
				}
			}
		}
		// getNickName();
	}
	
	private void drawTriangle(GraphicsContext gc, MouseEvent e, double x, double y){
		gc.setFill(Color.WHITE);
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		gc.strokeLine(x, y, e.getX(), e.getY());
		double xx = e.getX() - x;
		gc.strokeLine(e.getX(), e.getY(), e.getX() -xx -xx , e.getY());
		gc.strokeLine(e.getX() -xx -xx , e.getY(), x, y);
		trianglePointsArrayList.clear();
		trianglePointsArrayList.add(x);
		trianglePointsArrayList.add(y);
		trianglePointsArrayList.add(e.getX());
		trianglePointsArrayList.add(e.getY());
		trianglePointsArrayList.add(e.getX()-xx-xx);
		trianglePointsArrayList.add(e.getY());
		trianglePointsArrayList.add(x);
		trianglePointsArrayList.add(y);
		triangle.setStroke(color);
		triangle.setStrokeWidth(lineWidth);
		triangle.getPoints().clear();
		triangle.getPoints().addAll(trianglePointsArrayList);
	}

	private void drawSelection(GraphicsContext gc, ArrayList<Node> shape) {
		if (shape.size() != 0) {
			double[] selectionBoundsArrayX = new double[5];
			double[] selectionBoundsArrayY = new double[5];
			Bounds firstShapeBounds = shape.get(0).getBoundsInParent();
			double selMinX = firstShapeBounds.getMinX();
			double selMinY = firstShapeBounds.getMinY();
			double selMaxX = firstShapeBounds.getMaxX();
			double selMaxY = firstShapeBounds.getMaxY();
			for (int i = 1; i < shape.size(); i++) {
				Bounds selectionBounds = shape.get(i).getBoundsInParent();
				if (selMinX > selectionBounds.getMinX()) {
					selMinX = selectionBounds.getMinX();
				}
				if (selMinY > selectionBounds.getMinY()) {
					selMinY = selectionBounds.getMinY();
				}
				if (selMaxX < selectionBounds.getMaxX()) {
					selMaxX = selectionBounds.getMaxX();
				}
				if (selMaxY < selectionBounds.getMaxY()) {
					selMaxY = selectionBounds.getMaxY();
				}
			}
			selectionBoundsArrayX[0] = selMinX - 2;
			selectionBoundsArrayX[1] = selMaxX + 2;
			selectionBoundsArrayX[2] = selMaxX + 2;
			selectionBoundsArrayX[3] = selMinX - 2;
			selectionBoundsArrayX[4] = selMinX - 2;

			selectionBoundsArrayY[0] = selMinY - 2;
			selectionBoundsArrayY[1] = selMinY - 2;
			selectionBoundsArrayY[2] = selMaxY + 2;
			selectionBoundsArrayY[3] = selMaxY + 2;
			selectionBoundsArrayY[4] = selMinY - 2;

			gc.setStroke(Color.RED);
			gc.setLineWidth(1);
			gc.setLineDashes(new double[] { 5.0, 5.0 });
			gc.strokePolyline(selectionBoundsArrayX, selectionBoundsArrayY, selectionBoundsArrayX.length);
		}
		// FINISH make the line gap, and maybe animate;
	}

	private double[] drawDragginSelection(GraphicsContext gc, MouseEvent e) {
		gc.setFill(Color.rgb(242, 242, 242));
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(1);
		gc.setLineDashes(new double[] { 0.0, 5.0, 10.0, 5.0 });
		if (e.getX() < startX && e.getY() < startY) {
			gc.strokeRect(e.getX(), e.getY(), startX - e.getX(), startY - e.getY());
			return new double[] { e.getX(), e.getY(), startX - e.getX(), startY - e.getY() };
		} else if (e.getX() < startX) {
			gc.strokeRect(e.getX(), startY, startX - e.getX(), e.getY() - startY);
			return new double[] { e.getX(), startY, startX - e.getX(), e.getY() - startY };
		} else if (e.getY() < startY) {
			gc.strokeRect(startX, e.getY(), e.getX() - startX, startY - e.getY());
			return new double[] { startX, e.getY(), e.getX() - startX, startY - e.getY() };
		} else {
			gc.strokeRect(startX, startY, e.getX() - startX, e.getY() - startY);
			return new double[] { startX, startY, e.getX() - startX, e.getY() - startY };
		}
	}

	private void drawLine(GraphicsContext gc, MouseEvent e) {
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		gc.strokeLine(startX, startY, e.getX(), e.getY());
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(e.getX());
		line.setEndY(e.getY());
	}

	private void drawDottedLine(GraphicsContext gc, MouseEvent e) {
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		gc.setLineCap(StrokeLineCap.ROUND);
		gc.setLineDashes(new double[] { 1, 10 });
		gc.strokeLine(startX, startY, e.getX(), e.getY());
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(e.getX());
		line.setEndY(e.getY());
		line.setStrokeLineCap(StrokeLineCap.ROUND);
		line.getStrokeDashArray().addAll(1d, 10d);
		gc.setLineCap(null);
		gc.setLineDashes(null);
	}

	private void drawDashedLine(GraphicsContext gc, MouseEvent e) {
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		gc.setLineDashes(new double[] { 4, 10 });
		gc.strokeLine(startX, startY, e.getX(), e.getY());
		line.setStartX(startX);
		line.setStartY(startY);
		line.setEndX(e.getX());
		line.setEndY(e.getY());
		line.getStrokeDashArray().addAll(4d, 10d);
		gc.setLineCap(null);
		gc.setLineDashes(null);
	}

	private void drawRectangle(GraphicsContext gc, MouseEvent e) {
		gc.setFill(Color.WHITE);
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		if (e.getX() < startX && e.getY() < startY) {
			gc.strokeRect(e.getX(), e.getY(), startX - e.getX(), startY - e.getY());
			setRectangleXYWidthHeight(e.getX(), e.getY(), startX - e.getX(), startY - e.getY());
		} else if (e.getX() < startX) {
			gc.strokeRect(e.getX(), startY, startX - e.getX(), e.getY() - startY);
			setRectangleXYWidthHeight(e.getX(), startY, startX - e.getX(), e.getY() - startY);
		} else if (e.getY() < startY) {
			gc.strokeRect(startX, e.getY(), e.getX() - startX, startY - e.getY());
			setRectangleXYWidthHeight(startX, e.getY(), e.getX() - startX, startY - e.getY());
		} else {
			gc.strokeRect(startX, startY, e.getX() - startX, e.getY() - startY);
			setRectangleXYWidthHeight(startX, startY, e.getX() - startX, e.getY() - startY);
		}
	}

	private void drawDot(GraphicsContext gc, MouseEvent e) {
		gc.setFill(color);
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		gc.strokeOval(e.getX(), e.getY(), lineWidth, lineWidth);
	}

	private void drawEllipse(GraphicsContext gc, MouseEvent e) {
		gc.setFill(Color.WHITE);
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		if (e.getX() < startX && e.getY() < startY) {
			gc.strokeOval(e.getX(), e.getY(), startX - e.getX(), startY - e.getY());
			setEllipseXYWidthHeight(e.getX(), e.getY(), startX - e.getX(), startY - e.getY());
		} else if (e.getX() < startX) {
			gc.strokeOval(e.getX(), startY, startX - e.getX(), e.getY() - startY);
			setEllipseXYWidthHeight(e.getX(), startY, startX - e.getX(), e.getY() - startY);
		} else if (e.getY() < startY) {
			gc.strokeOval(startX, e.getY(), e.getX() - startX, startY - e.getY());
			setEllipseXYWidthHeight(startX, e.getY(), e.getX() - startX, startY - e.getY());
		} else {
			gc.strokeOval(startX, startY, e.getX() - startX, e.getY() - startY);
			setEllipseXYWidthHeight(startX, startY, e.getX() - startX, e.getY() - startY);
		}
	}

	private void drawPolygonPolyline(GraphicsContext gc, MouseEvent e, double x, double y) {
		gc.setStroke(color);
		gc.setLineWidth(lineWidth);
		if (tempX <= -1 && tempY <= -1) {
			tempX = x;
			tempY = y;
		} else {
			gc.strokeLine(tempX, tempY, x, y);
			tempX = x;
			tempY = y;
		}
	}

	private void setRectangleXYWidthHeight(double startX, double startY, double width, double height) {
		rect.setX(startX);
		rect.setY(startY);
		rect.setWidth(width);
		rect.setHeight(height);
	}

	private void setEllipseXYWidthHeight(double startX, double startY, double width, double height) {
		ellipse.setCenterX(startX + width / 2);
		ellipse.setCenterY(startY + height / 2);
		ellipse.setRadiusX(width / 2);
		ellipse.setRadiusY(height / 2);
	}

	private void clearCanvas(GraphicsContext gc, Canvas canvas) {
		gc.setFill(Color.rgb(242, 242, 242));
		gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(1.0);
		gc.setLineDashes(0);
		// gc.strokeText("X: " + e.getX(), canvas.getWidth() - 200,
		// canvas.getHeight() - 50);
		// gc.strokeText("Y: " + e.getY(), canvas.getWidth() - 200,
		// canvas.getHeight() - 30);
	}

	private void resetGC() {

	}

	public void copy(Group group, TreeItem<AllProperties> treeItem) {
		copiedShapesList.clear();
		for (int i = 0; i < selectedShapesList.size(); i++) {
			if (!(selectedShapesList.get(i) instanceof Canvas)) {
				copiedShapesList.add(selectedShapesList.get(i));
			}
		}
		for (int i = 0; i < copiedShapesList.size(); i++) {
			System.out.println("copiedList{" + i + "] = " + copiedShapesList.get(i));
		}
		System.out.println("copiedList i : " + copiedShapesList.size());
	}

	public void paste(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		if (copiedShapesList != null) {
			for (int i = 0; i < copiedShapesList.size(); i++) {
				if (!(copiedShapesList.get(i) instanceof Canvas)) {
					/*
					 * if(copiedShapesList.get(i) instanceof Line){ Line l =
					 * (Line)copiedShapesList.get(i); }else
					 * if(copiedShapesList.get(i) instanceof Rectangle){
					 * Rectangle r = new Rectangle(); //r =
					 * (Rectangle)copiedShapesList.get(i); //r =
					 * (Rectangle)copiedShapesList.get(i).getProperties();
					 * ObservableMap<Object,Object> map1 = new map
					 * System.out.println(copiedShapesList.get(i).getProperties(
					 * )); //group.getChildren().add(r); }else
					 * if(copiedShapesList.get(i) instanceof Ellipse){ Ellipse e
					 * = (Ellipse)copiedShapesList.get(i);
					 * group.getChildren().add(e); }else
					 * if(copiedShapesList.get(i) instanceof Polyline){ Polyline
					 * pl = (Polyline)copiedShapesList.get(i);
					 * group.getChildren().add(pl); }else
					 * if(copiedShapesList.get(i) instanceof Polygon){ Polygon
					 * pg = (Polygon)copiedShapesList.get(i);
					 * group.getChildren().add(pg); }
					 */
					group.getChildren().add(copiedShapesList.get(i));
				}
			}
		}
		clearCanvas(gc, canvas);
		treeViewUpdate(group, treeItem);
	}

	public void cut(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		copiedShapesList.clear();
		for (int i = 0; i < selectedShapesList.size(); i++) {
			if (!(selectedShapesList.get(i) instanceof Canvas)) {
				copiedShapesList.add(selectedShapesList.get(i));
				group.getChildren().remove(selectedShapesList.get(i));
			}
		}
		for (int i = 0; i < copiedShapesList.size(); i++) {
			System.out.println("copiedList{" + i + "] = " + copiedShapesList.get(i));
		}
		System.out.println("copiedList i : " + copiedShapesList.size());

		clearCanvas(gc, canvas);
		treeViewUpdate(group, treeItem);
	}

	public void deleteAllShapes(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		for (int i = group.getChildren().size() - 1; i >= 1; i--) {
			group.getChildren().remove(i);
		}
		treeItem.getChildren().clear();
		clearCanvas(gc, canvas);
	}

	public void deleteSelection(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				group.getChildren().remove(selectedShapesList.get(i));
			}
		}
		clearCanvas(gc, canvas);
		treeViewUpdate(group, treeItem);
	}

	public void selectAll(GraphicsContext gc, Group group, Canvas canvas) {
		selectedShapesList.clear();
		translateXList.clear();
		translateYList.clear();
		for (int i = 0; i < group.getChildren().size(); i++) {
			if (!(group.getChildren().get(i) instanceof Canvas)) {
				selectedShapesList.add(group.getChildren().get(i));
				translateXList.add(group.getChildren().get(i).getTranslateX());
				translateYList.add(group.getChildren().get(i).getTranslateY());
			}
		}
		// for (int i = 0; i < selectedShapesList.size(); i++) {
		// System.out.println("selectedShape [" + i + "] = " +
		// selectedShapesList.get(i));
		// }
		// System.out.println("selectedShapes i: " + selectedShapesList.size());
		clearCanvas(gc, canvas);
		drawSelection(gc, selectedShapesList);
	}

	public void deselectAll(GraphicsContext gc, Canvas canvas) {
		selectedShapesList.clear();
		clearCanvas(gc, canvas);
	}

	private void createAndAddPropertyToHashMap(Node node) {
		AllProperties property = createProperty(node);
		nodeToAllPropertiesMap.put(node, property);
	}

	private AllProperties createProperty(Node node) {
		if (!(node instanceof Canvas)) {
			savingPropertiesObject = new AllProperties();
			if (node instanceof Group) {
				savingPropertiesObject.setShapeName("Group");
				savingPropertiesObject.setGroup(true);
			} else {
				if (node instanceof Rectangle) {
					Rectangle rect = (Rectangle) node;
					savingPropertiesObject.setShapeName("Rectangle");
					savingPropertiesObject.setX(rect.getX());
					savingPropertiesObject.setY(rect.getY());
					savingPropertiesObject.setWidth(rect.getWidth());
					savingPropertiesObject.setHeight(rect.getHeight());
					savingPropertiesObject.setFillColor(rect.getFill().toString());
				}
				if (node instanceof Line) {
					Line line = (Line) node;
					savingPropertiesObject.setShapeName("Line");
					savingPropertiesObject.setStartX(line.getStartX());
					savingPropertiesObject.setStartY(line.getStartY());
					savingPropertiesObject.setEndX(line.getEndX());
					savingPropertiesObject.setEndY(line.getEndY());
				}
				if (node instanceof Ellipse) {
					Ellipse ellipse = (Ellipse) node;
					savingPropertiesObject.setShapeName("Ellipse");
					savingPropertiesObject.setCenterX(ellipse.getCenterX());
					savingPropertiesObject.setCenterY(ellipse.getCenterY());
					savingPropertiesObject.setRadiusX(ellipse.getRadiusX());
					savingPropertiesObject.setRadiusY(ellipse.getRadiusY());
					savingPropertiesObject.setFillColor(ellipse.getFill().toString());
				}
				if (node instanceof Circle) {
					Circle circle = (Circle) node;
					savingPropertiesObject.setShapeName("Dot");
					savingPropertiesObject.setCenterX(circle.getCenterX());
					savingPropertiesObject.setCenterY(circle.getCenterY());
					savingPropertiesObject.setRadius(circle.getRadius());
					savingPropertiesObject.setFillColor(circle.getFill().toString());
				}
				if (node instanceof Polygon) {
					Polygon poly = (Polygon) node;
					savingPropertiesObject.setShapeName("Polygon");
					savingPropertiesObject.setFillColor(poly.getFill().toString());
					savingPropertiesObject.setPolyPointsArrayList(poly.getPoints());
				}
				if (node instanceof Polyline) {
					Polyline poly = (Polyline) node;
					savingPropertiesObject.setShapeName("Polyline");
					savingPropertiesObject.setPolyPointsArrayList(poly.getPoints());
				}
				Shape shape = (Shape) node;
				savingPropertiesObject.setGroup(false);
				savingPropertiesObject.setStrokeDashArray(ObservableListToArrayList(shape.getStrokeDashArray()));
				savingPropertiesObject.setStrokeLineCap(shape.getStrokeLineCap());
				savingPropertiesObject.setLineWidth(shape.getStrokeWidth());
				savingPropertiesObject.setStrokeColor(shape.getStroke().toString());
				savingPropertiesObject.setRotate(shape.getRotate());
				savingPropertiesObject.setScaleX(shape.getScaleX());
				savingPropertiesObject.setScaleY(shape.getScaleY());
			}
			return savingPropertiesObject;
		}
		return null;
	}

	/*
	 * private void savingProperties(Group group) { for (int i = 0; i <
	 * group.getChildren().size(); i++) { System.out.println("group[" + i +
	 * "] = " + group.getChildren().get(i).toString()); if
	 * (!(group.getChildren().get(i) instanceof Canvas)) {
	 * savingPropertiesObject = new AllProperties(); if
	 * (group.getChildren().get(i) instanceof Group) {
	 * savingPropertiesObject.setShapeName("Group");
	 * savingPropertiesObject.setGroup(true);
	 * savingArrayList.add(savingPropertiesObject); savingProperties((Group)
	 * (group.getChildren().get(i))); } else { if (group.getChildren().get(i)
	 * instanceof Rectangle) { Rectangle rect = (Rectangle)
	 * group.getChildren().get(i);
	 * savingPropertiesObject.setShapeName("Rectangle");
	 * savingPropertiesObject.setX(rect.getX());
	 * savingPropertiesObject.setY(rect.getY());
	 * savingPropertiesObject.setWidth(rect.getWidth());
	 * savingPropertiesObject.setHeight(rect.getHeight());
	 * savingPropertiesObject.setFillColor(rect.getFill().toString()); } if
	 * (group.getChildren().get(i) instanceof Line) { Line line = (Line)
	 * group.getChildren().get(i); savingPropertiesObject.setShapeName("Line");
	 * savingPropertiesObject.setStartX(line.getStartX());
	 * savingPropertiesObject.setStartY(line.getStartY());
	 * savingPropertiesObject.setEndX(line.getEndX());
	 * savingPropertiesObject.setEndY(line.getEndY()); } if
	 * (group.getChildren().get(i) instanceof Ellipse) { Ellipse ellipse =
	 * (Ellipse) group.getChildren().get(i);
	 * savingPropertiesObject.setShapeName("Ellipse");
	 * savingPropertiesObject.setCenterX(ellipse.getCenterX());
	 * savingPropertiesObject.setCenterY(ellipse.getCenterY());
	 * savingPropertiesObject.setRadiusX(ellipse.getRadiusX());
	 * savingPropertiesObject.setRadiusY(ellipse.getRadiusY());
	 * savingPropertiesObject.setFillColor(ellipse.getFill().toString()); } if
	 * (group.getChildren().get(i) instanceof Circle) { Circle circle = (Circle)
	 * group.getChildren().get(i); savingPropertiesObject.setShapeName("Dot");
	 * savingPropertiesObject.setCenterX(circle.getCenterX());
	 * savingPropertiesObject.setCenterY(circle.getCenterY());
	 * savingPropertiesObject.setRadius(circle.getRadius());
	 * savingPropertiesObject.setFillColor(circle.getFill().toString()); } if
	 * (group.getChildren().get(i) instanceof Polygon) { Polygon poly =
	 * (Polygon) group.getChildren().get(i);
	 * savingPropertiesObject.setShapeName("Polygon");
	 * savingPropertiesObject.setFillColor(poly.getFill().toString());
	 * savingPropertiesObject.setPolyPointsArrayList(poly.getPoints()); } if
	 * (group.getChildren().get(i) instanceof Polyline) { Polyline poly =
	 * (Polyline) group.getChildren().get(i);
	 * savingPropertiesObject.setShapeName("Polyline");
	 * savingPropertiesObject.setPolyPointsArrayList(poly.getPoints()); } Shape
	 * shape = (Shape) group.getChildren().get(i);
	 * savingPropertiesObject.setGroup(false);
	 * savingPropertiesObject.setStrokeDashArray(ObservableListToArrayList(shape
	 * .getStrokeDashArray()));
	 * savingPropertiesObject.setStrokeLineCap(shape.getStrokeLineCap());
	 * savingPropertiesObject.setLineWidth(shape.getStrokeWidth());
	 * savingPropertiesObject.setStrokeColor(shape.getStroke().toString());
	 * savingPropertiesObject.setRotate(shape.getRotate());
	 * savingPropertiesObject.setScaleX(shape.getScaleX());
	 * savingPropertiesObject.setScaleY(shape.getScaleY());
	 * savingArrayList.add(savingPropertiesObject); }
	 * nodeToAllPropertiesMap.put(group.getChildren().get(i),
	 * savingPropertiesObject); } } }
	 */

	private ArrayList<Double> ObservableListToArrayList(ObservableList<Double> strokeDashArray) {
		ArrayList<Double> list = new ArrayList<>();
		for (Double item : strokeDashArray) {
			list.add(item);
		}
		return list;
	}

	public void save(Group group) {

		System.out.println("" + group.getChildren().size());
		for (Node node : group.getChildren()) {
			if (!(node instanceof Canvas)) {
				System.out.println(node);
			}
		}
		// Call method for all of this
		ArrayList<AllProperties> properties = new ArrayList<>();
		for (int i = 0; i < group.getChildren().size(); i++) {
			if (!(group.getChildren().get(i) instanceof Canvas)) {
				if (group.getChildren().get(i) instanceof Group) {
					Group currentGroup = (Group) group.getChildren().get(i);
					nodeToAllPropertiesMap.get(group.getChildren().get(i))
							.setGroupSize(currentGroup.getChildren().size());
				}
			}
		}
		properties.addAll(nodeToAllPropertiesMap.values());
		for (AllProperties p : properties) {
			System.out.println("before saving: " + p.getNickName() + " group size: " + p.getGroupSize());
		}
		// till here
		try {
			File file = new File("tmp/saved.ser");
			if (file.exists()) {
				file.delete();
			}
			FileOutputStream fileOut = new FileOutputStream("tmp/saved.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(properties);
			out.close();
			fileOut.close();
			System.out.println("File saved succesfully");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void open(Group group, TreeItem<AllProperties> rootItem) {
		nodeToAllPropertiesMap.clear();
		ArrayList<AllProperties> properties = new ArrayList<>();
		try {
			FileInputStream fileIn = new FileInputStream("tmp/saved.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			properties = (ArrayList<AllProperties>) in.readObject();
			in.close();
			fileIn.close();
			System.out.println("File opened succesfully");
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			System.out.println("Class not found");
			c.printStackTrace();
			return;
		}
		for (AllProperties p : properties) {
			System.out.println("after opening: " + p.getNickName() + " group size: " + p.getGroupSize());
		}
		savingToGroup(group, properties);
		savingToTreeView(0, properties, rootItem, group);
		treeViewUpdate(group, rootItem);
	}

	public void savingToGroup(Group group, ArrayList<AllProperties> properties) {
		for (int i = 0; i < properties.size(); i++) {

		}
	}

	public void savingToTreeView(int index, ArrayList<AllProperties> propertiesArrayList,
			TreeItem<AllProperties> treeItem, Group group) {
		for (int i = index; i < propertiesArrayList.size(); i++) {
			if (propertiesArrayList.get(i).isGroup()) {
				savingToTreeView(i, propertiesArrayList, treeItem, group);
			} else {
				if (propertiesArrayList.get(i).getShapeName().equals("Rectangle")) {
					AllProperties properties = propertiesArrayList.get(i);
					Rectangle rect = new Rectangle();
					rect.setX(properties.getX());
					rect.setY(properties.getY());
					rect.setWidth(properties.getWidth());
					rect.setHeight(properties.getHeight());
					rect.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					rect.setStrokeLineCap(properties.getStrokeLineCap());
					rect.setStrokeWidth(properties.getLineWidth());
					rect.setFill(Color.valueOf(properties.getFillColor()));
					rect.setStroke(Color.valueOf(properties.getStrokeColor()));
					rect.setRotate(properties.getRotate());
					rect.setScaleX(properties.getScaleX());
					rect.setScaleY(properties.getScaleY());
					group.getChildren().add(rect);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Line")) {
					AllProperties properties = propertiesArrayList.get(i);
					Line line = new Line();
					line.setStartX(properties.getStartX());
					line.setStartY(properties.getStartY());
					line.setEndX(properties.getEndX());
					line.setEndY(properties.getEndY());
					line.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					line.setStrokeLineCap(properties.getStrokeLineCap());
					line.setStrokeWidth(properties.getLineWidth());
					line.setStroke(Color.valueOf(properties.getStrokeColor()));
					line.setRotate(properties.getRotate());
					line.setScaleX(properties.getScaleX());
					line.setScaleY(properties.getScaleY());
					group.getChildren().add(line);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Ellipse")) {
					AllProperties properties = propertiesArrayList.get(i);
					Ellipse ellipse = new Ellipse();
					ellipse.setCenterX(properties.getCenterX());
					ellipse.setCenterY(properties.getCenterY());
					ellipse.setRadiusX(properties.getRadiusX());
					ellipse.setRadiusY(properties.getRadiusY());
					ellipse.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					ellipse.setStrokeLineCap(properties.getStrokeLineCap());
					ellipse.setStrokeWidth(properties.getLineWidth());
					ellipse.setFill(Color.valueOf(properties.getFillColor()));
					ellipse.setStroke(Color.valueOf(properties.getStrokeColor()));
					ellipse.setRotate(properties.getRotate());
					ellipse.setScaleX(properties.getScaleX());
					ellipse.setScaleY(properties.getScaleY());
					group.getChildren().add(ellipse);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Dot")) {
					AllProperties properties = propertiesArrayList.get(i);
					Circle circle = new Circle();
					circle.setCenterX(properties.getCenterX());
					circle.setCenterY(properties.getCenterY());
					circle.setRadius(properties.getRadius());
					circle.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					circle.setStrokeLineCap(properties.getStrokeLineCap());
					circle.setStrokeWidth(properties.getLineWidth());
					circle.setFill(Color.valueOf(properties.getFillColor()));
					circle.setStroke(Color.valueOf(properties.getStrokeColor()));
					circle.setRotate(properties.getRotate());
					circle.setScaleX(properties.getScaleX());
					circle.setScaleY(properties.getScaleY());
					group.getChildren().add(circle);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Polygon")) {
					AllProperties properties = propertiesArrayList.get(i);
					Polygon polygon = new Polygon();
					polygon.getPoints().addAll(properties.getPolyPointsArrayList());
					polygon.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					polygon.setStrokeLineCap(properties.getStrokeLineCap());
					polygon.setStrokeWidth(properties.getLineWidth());
					polygon.setFill(Color.valueOf(properties.getFillColor()));
					polygon.setStroke(Color.valueOf(properties.getStrokeColor()));
					polygon.setRotate(properties.getRotate());
					polygon.setScaleX(properties.getScaleX());
					polygon.setScaleY(properties.getScaleY());
					group.getChildren().add(polygon);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Polyline")) {
					AllProperties properties = propertiesArrayList.get(i);
					Polyline polyline = new Polyline();
					polyline.getPoints().addAll(properties.getPolyPointsArrayList());
					polyline.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					polyline.setStrokeLineCap(properties.getStrokeLineCap());
					polyline.setStrokeWidth(properties.getLineWidth());
					polyline.setStroke(Color.valueOf(properties.getStrokeColor()));
					polyline.setRotate(properties.getRotate());
					polyline.setScaleX(properties.getScaleX());
					polyline.setScaleY(properties.getScaleY());
					group.getChildren().add(polyline);
				}
				if (propertiesArrayList.get(i).getShapeName().equals("Triangle")) {
					AllProperties properties = propertiesArrayList.get(i);
					Polyline triangle = new Polyline();
					triangle.getPoints().addAll(properties.getPolyPointsArrayList());
					triangle.getStrokeDashArray().addAll((properties.getStrokeDashArray()));
					triangle.setStrokeLineCap(properties.getStrokeLineCap());
					triangle.setStrokeWidth(properties.getLineWidth());
					triangle.setStroke(Color.valueOf(properties.getStrokeColor()));
					triangle.setRotate(properties.getRotate());
					triangle.setScaleX(properties.getScaleX());
					triangle.setScaleY(properties.getScaleY());
					group.getChildren().add(triangle);
				}
			}
		}
	}

	public void controlPressed() {
		isControl = false;
	}

	public void enterPressed(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		isControl = false;
		if (polygon != null) {
			polygon.setFill(color);
			polygon.setStroke(color);
			polygon.setStrokeWidth(lineWidth);
			polygon.getPoints().addAll(polyPointsArrayList);
			clearCanvas(gc, canvas);
			group.getChildren().add(polygon);
			createAndAddPropertyToHashMap(polygon);
			polygon = null;
			polyPointsArrayList.clear();
			tempX = -1;
			tempY = -1;
			treeViewUpdate(group, treeItem);
		}
		if (polyline != null) {
			polyline.setStroke(color);
			polyline.setStrokeWidth(lineWidth);
			polyline.getPoints().addAll(polyPointsArrayList);
			clearCanvas(gc, canvas);
			group.getChildren().add(polyline);
			createAndAddPropertyToHashMap(polyline);
			polyline = null;
			polyPointsArrayList.clear();
			tempX = -1;
			tempY = -1;
			treeViewUpdate(group, treeItem);
		}
	}

	public boolean isControl() {
		return isControl;
	}

	public void setControl(boolean isControl) {
		this.isControl = isControl;
	}

	public void setTool(String name) {
		this.toolName = name;
	}

	public String getTool() {
		return toolName;
	}

	public void setLineWidth(GraphicsContext gc, Canvas canvas, double lineWidth) {
		this.lineWidth = lineWidth;
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				if (selectedShapesList.get(i) instanceof Group) {
					for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setLineWidth(lineWidth, ((Group) node).getChildren());
						} else {
							((Shape) node).setStrokeWidth(lineWidth);
						}
					}
				} else {
					((Shape) selectedShapesList.get(i)).setStrokeWidth(lineWidth);
				}
			}
			clearCanvas(gc, canvas);
			drawSelection(gc, selectedShapesList);
		}
	}

	private void setLineWidth(double lineWidth, ObservableList<Node> observableList) {
		if (observableList != null) {
			for (int i = 0; i < observableList.size(); i++) {
				if (observableList.get(i) instanceof Group) {
					for (Node node : ((Group) observableList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setLineWidth(lineWidth, ((Group) node).getChildren());
						} else {
							((Shape) node).setStrokeWidth(lineWidth);
						}
					}
				} else {
					((Shape) observableList.get(i)).setStrokeWidth(lineWidth);
				}
			}
		}
	}

	public double getLineWidth() {
		return lineWidth;
	}

	public void setRotate(GraphicsContext gc, Canvas canvas, double deg) {
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				if (selectedShapesList.get(i) instanceof Group) {
					for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setRotate(deg, ((Group) node).getChildren());
						} else {
							rotation = node.getRotate();
							rotation += deg;
							node.setRotate(rotation);
						}
					}

				} else {
					rotation = selectedShapesList.get(i).getRotate();
					rotation += deg;
					selectedShapesList.get(i).setRotate(rotation);
				}
			}
			clearCanvas(gc, canvas);
			drawSelection(gc, selectedShapesList);
		}
	}

	private void setRotate(double deg, ObservableList<Node> observableList) {
		if (observableList != null) {
			for (int i = 0; i < observableList.size(); i++) {
				if (observableList.get(i) instanceof Group) {
					for (Node node : ((Group) observableList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setRotate(deg, ((Group) node).getChildren());
						} else {
							rotation = node.getRotate();
							rotation += deg;
							node.setRotate(rotation);
						}
					}
				} else {
					rotation = observableList.get(i).getRotate();
					rotation += deg;
					observableList.get(i).setRotate(rotation);
				}
			}
		}
	}

	public double getRotate() {
		return rotation;
	}

	public void setScale(GraphicsContext gc, Canvas canvas, double scale) {
		scale = scale / 100;
		double scaleX, scaleY;
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				if (selectedShapesList.get(i) instanceof Group) {
					for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setScale(scale, ((Group) node).getChildren());
						} else {
							scaleX = node.getScaleX();
							scaleX += scale;
							scaleY = node.getScaleY();
							scaleY += scale;
							node.setScaleX(scaleX);
							node.setScaleY(scaleY);
						}
					}
				} else {
					scaleX = selectedShapesList.get(i).getScaleX();
					scaleX += scale;
					scaleY = selectedShapesList.get(i).getScaleY();
					scaleY += scale;
					selectedShapesList.get(i).setScaleX(scaleX);
					selectedShapesList.get(i).setScaleY(scaleY);
				}
			}
			clearCanvas(gc, canvas);
			drawSelection(gc, selectedShapesList);
		}
	}

	private void setScale(double scale, ObservableList<Node> observableList) {
		double scaleX, scaleY;
		if (observableList != null) {
			for (int i = 0; i < observableList.size(); i++) {
				if (observableList.get(i) instanceof Group) {
					for (Node node : ((Group) observableList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setScale(scale, ((Group) node).getChildren());
						} else {
							scaleX = node.getScaleX();
							scaleX += scale;
							scaleY = node.getScaleY();
							scaleY += scale;
							node.setScaleX(scaleX);
							node.setScaleY(scaleY);
						}
					}
				} else {
					scaleX = observableList.get(i).getScaleX();
					scaleX += scale;
					scaleY = observableList.get(i).getScaleY();
					scaleY += scale;
					observableList.get(i).setScaleX(scaleX);
					observableList.get(i).setScaleY(scaleY);
				}
			}
		}
	}

	public void setGroup(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		if (selectedShapesList.size() != 0) {
			Group newGroup = new Group();
			for (int i = 0; i < selectedShapesList.size(); i++) {
				group.getChildren().remove(selectedShapesList.get(i));
				newGroup.getChildren().add(selectedShapesList.get(i));
			}
			group.getChildren().add(newGroup);
			createAndAddPropertyToHashMap(newGroup);
			treeViewUpdate(group, treeItem);
		}
		clearCanvas(gc, canvas);
		treeViewUpdate(group, treeItem);
		selectedShapesList.clear();
	}

	public void removeGroup(GraphicsContext gc, Canvas canvas, Group group, TreeItem<AllProperties> treeItem) {
		Group newGroup = new Group();
		double translateX = 0;
		double translateY = 0;
		if (selectedShapesList != null) {
			for (Node node : selectedShapesList) {
				if (node instanceof Group) {
					removeGroup((Group) node, newGroup, translateX, translateY);
				} else {
					newGroup.getChildren().add(node);
				}
			}
			while (newGroup.getChildren().size() != 0) {
				group.getChildren().add(newGroup.getChildren().get(0));
			}
			for (int i = 0; i < selectedShapesList.size(); i++) {
				System.out.println("selectedShapesList.get(i)" + selectedShapesList.get(i));
				if (selectedShapesList.get(i) instanceof Group)
					group.getChildren().remove(selectedShapesList.get(i));
			}
			selectedShapesList.clear();
		}
		clearCanvas(gc, canvas);
		treeViewUpdate(group, treeItem);
	}

	private void removeGroup(Group groupNode, Group newGroup, double translateX, double translateY) {
		translateX += groupNode.getTranslateX();
		translateY += groupNode.getTranslateY();
		System.out.println("groupNode translate: " + translateX);
		for (int i = groupNode.getChildren().size() - 1; i >= 0; i--) {
			if (groupNode.getChildren().get(i) instanceof Group) {
				removeGroup((Group) groupNode.getChildren().get(i), newGroup, translateX, translateY);
			} else {
				groupNode.getChildren().get(i)
						.setTranslateX(groupNode.getChildren().get(i).getTranslateX() + translateX);
				groupNode.getChildren().get(i)
						.setTranslateY(groupNode.getChildren().get(i).getTranslateY() + translateY);
				newGroup.getChildren().add(groupNode.getChildren().get(i));
			}
		}
	}

	public void printSelectedShapesList() {
		System.out.println("---------------------------");
		System.out.println("selectedShapesList.size(): " + selectedShapesList.size());
		for (int i = 0; i < selectedShapesList.size(); i++) {
			if (selectedShapesList.get(i) instanceof Group) {
				System.out.println("selectedShapesList [" + i + "] = " + selectedShapesList.get(i));
				printGroup((Group) selectedShapesList.get(i));
			} else {
				System.out.println("selectedShapesList [" + i + "] = " + selectedShapesList.get(i));
			}
		}
		System.out.println("---------------------------");
	}

	public void printGroup(Group group) {
		for (int i = 0; i < group.getChildren().size(); i++) {
			if (group.getChildren().get(i) instanceof Group) {
				System.out.println("group.getChildren [" + i + "] = " + group.getChildren().get(i));
				printGroup((Group) group.getChildren().get(i));
			} else {
				System.out.println("group.getChildren [" + i + "] = " + group.getChildren().get(i));
			}
		}
	}

	public void setColor(GraphicsContext gc, Canvas canvas, Color value) {
		color = value;
	}

	private void setFillColor(Color color, ObservableList<Node> observableList) {
		if (observableList != null) {
			for (int i = 0; i < observableList.size(); i++) {
				if (observableList.get(i) instanceof Group) {
					for (Node node : ((Group) observableList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setFillColor(color, ((Group) node).getChildren());
						} else {
							((Shape) node).setFill(color);
						}
					}
				} else {
					((Shape) observableList.get(i)).setFill(color);
				}
			}
		}
	}

	private void setStrokeColor(Color color, ObservableList<Node> observableList) {
		if (observableList != null) {
			for (int i = 0; i < observableList.size(); i++) {
				if (observableList.get(i) instanceof Group) {
					for (Node node : ((Group) observableList.get(i)).getChildren()) {
						if (node instanceof Group) {
							setStrokeColor(color, ((Group) node).getChildren());
						} else {
							((Shape) node).setStroke(color);
						}
					}
				} else {
					((Shape) observableList.get(i)).setStroke(color);
				}
			}
		}
	}

	public void setDashWidth1(Number oldNumber, Number newNumber) {
		// napravi promenlivi v gui-to da se pomni old value na vseki slider, za
		// da moje da stane new and old.
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				if (selectedShapesList.get(i) instanceof Group) {
					for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
						if (node instanceof Group) {
							// setScale(scale, ((Group) node).getChildren());
						} else {
							((Line) node).getStrokeDashArray().clear();
							// ((Line) node).getStrokeDashArray().addAll(nmb);
							// ((Line) node).getStrokeDashArray().set(0,
							// newNumber.doubleValue());
						}
					}
				} else {
					// ((Line)
					// selectedShapesList.get(i)).getStrokeDashArray().clear();
					// ((Line)
					// selectedShapesList.get(i)).getStrokeDashArray().addAll(nmb,
					// 10d);
					// ((Line)
					// selectedShapesList.get(i)).getStrokeDashArray().set(0,
					// nmb);
				}
			}
			// drawSelection(gc, selectedShapesList);
		}
	}

	public void setDashWidth2(Number number) {
		double nmb = number.doubleValue();
		if (selectedShapesList != null) {
			for (int i = 0; i < selectedShapesList.size(); i++) {
				if (selectedShapesList.get(i) instanceof Group) {
					for (Node node : ((Group) selectedShapesList.get(i)).getChildren()) {
						if (node instanceof Group) {
							// setScale(scale, ((Group) node).getChildren());
						} else {
							// ((Line) node).getStrokeDashArray().clear();
							((Line) node).getStrokeDashArray().addAll(nmb);

						}
					}
				} else {
					// ((Line)
					// selectedShapesList.get(i)).getStrokeDashArray().clear();
					((Line) selectedShapesList.get(i)).getStrokeDashArray().addAll(nmb, 10d);
					// ((Line)
					// selectedShapesList.get(i)).getStrokeDashArray().set(1,
					// nmb);
				}
			}
		}
	}
}
