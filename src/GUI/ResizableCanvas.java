package GUI;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ResizableCanvas extends Canvas{
	
	private GraphicsContext gc;
	
	public ResizableCanvas(){
		widthProperty().addListener(evt -> draw());
		heightProperty().addListener(evt -> draw());
	}
	
	private void draw(){
		double width = getWidth();
		double height = getHeight();
		
		gc = getGraphicsContext2D();
		gc.clearRect(0, 0, width, height);
		
		gc.setStroke(Color.RED);
		gc.strokeLine(0, 0, width, height);
		gc.strokeLine(0, height, width, 0);
		System.out.println(width + " " + height);
	}
	
	public boolean isResizable(){
		return true;
	}
	
	public double prefWidth(double height){
		return getWidth();
	}
	
	public double prefHeight(double width){
		return getHeight();
	}
	
	public GraphicsContext getGC(){
		return this.gc;
	}
	
}
