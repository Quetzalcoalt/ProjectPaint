package GUI;

import java.util.Observable;
import java.util.Optional;
import java.util.Timer;

import javax.swing.text.html.parser.ParserDelegator;

import Drawing.Drawing;
import Utils.AllProperties;
import Utils.Constants;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

public class GUI extends Application {

	public GUI(Stage stage) {
		try {
			init();
			start(stage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage stage) throws Exception {
		Constants consts = new Constants();

		BorderPane rootNode = new BorderPane();

		// Menu start
		MenuBar menubar = new MenuBar();

		// Create the File menu.
		Menu fileMenu = new Menu("File");
		MenuItem newFile = new MenuItem("New");
		newFile.setAccelerator(KeyCombination.keyCombination("shortcut+N"));
		MenuItem open = new MenuItem("Open");
		open.setAccelerator(KeyCombination.keyCombination("shortcut+O"));
		MenuItem save = new MenuItem("Save");
		save.setAccelerator(KeyCombination.keyCombination("shortcut+S"));
		MenuItem exit = new MenuItem("Exit");
		fileMenu.getItems().addAll(newFile, open, save, new SeparatorMenuItem(), exit);

		// Create the edit menu/
		Menu editMenu = new Menu("Edit");

		MenuItem undo = new MenuItem("Undo");
		// make that only 1 is active at a time
		undo.setAccelerator(KeyCombination.keyCombination("shortcut+Z"));
		MenuItem redo = new MenuItem("Redo");
		redo.setAccelerator(KeyCombination.keyCombination("shortcut+Z"));
		MenuItem cut = new MenuItem("Cut");
		cut.setAccelerator(KeyCombination.keyCombination("shortcut+X"));
		MenuItem copy = new MenuItem("Copy");
		copy.setAccelerator(KeyCombination.keyCombination("shortcut+C"));
		MenuItem paste = new MenuItem("Paste");
		paste.setAccelerator(KeyCombination.keyCombination("shortcut+V"));
		MenuItem del = new MenuItem("Delete selection");
		del.setAccelerator(KeyCombination.keyCombination("Del"));
		MenuItem selectAll = new MenuItem("Select All");
		selectAll.setAccelerator(KeyCombination.keyCombination("shortcut+A"));
		MenuItem deselectAll = new MenuItem("Deselect All");
		deselectAll.setAccelerator(KeyCombination.keyCombination("shortcut+D"));

		editMenu.getItems().addAll(undo, redo, new SeparatorMenuItem(), cut, copy, paste, del, new SeparatorMenuItem(),
				selectAll, deselectAll);

		// Create Image menu.
		Menu imageMenu = new Menu("Image");

		MenuItem rotate = new MenuItem("Rotate");
		rotate.setAccelerator(KeyCombination.keyCombination("shortcut+R"));
		MenuItem scale = new MenuItem("Scale");
		scale.setAccelerator(KeyCombination.keyCombination("shortcut+H"));
		MenuItem nintyDegClockWise = new MenuItem("Rotate 90 deg Clockwise");
		MenuItem nintyDegCounterClockWise = new MenuItem("Rotate 90 deg Counter-Clockwise");
		MenuItem oneEightyDegClockWise = new MenuItem("Rotate 180 deg ");

		imageMenu.getItems().addAll(rotate, scale, new SeparatorMenuItem(), nintyDegClockWise, nintyDegCounterClockWise,
				oneEightyDegClockWise);

		// Create the About menu.
		Menu aboutMenu = new Menu("About");
		MenuItem about = new MenuItem("About");
		aboutMenu.getItems().addAll(about);

		menubar.getMenus().addAll(fileMenu, editMenu, imageMenu, aboutMenu);

		// Menu end

		// Dialogs start

		TextInputDialog tIDialogRotateAngle = new TextInputDialog();
		tIDialogRotateAngle.setHeaderText("Write down the angle you want to rotate your shape!");
		tIDialogRotateAngle.setTitle("Roration angle");
		tIDialogRotateAngle.setContentText("Write your number here: ");

		TextInputDialog tIDialogScaleSize = new TextInputDialog();
		tIDialogScaleSize.setHeaderText("Write down the percentage for scaling your shape!");
		tIDialogScaleSize.setTitle("Scale");
		tIDialogScaleSize.setContentText("Write your number here: ");

		// Color Picker
		final ColorPicker colorPicker = new ColorPicker(Color.BLACK);
		final Text colorPickerText = new Text("Select a color");
		colorPickerText.setFill(colorPicker.getValue());
		colorPicker.setTooltip(new Tooltip("Select a color"));

		// Canvas
		final Canvas canvas = new Canvas(800, 500);
		final GraphicsContext gc = canvas.getGraphicsContext2D();
		

		// ResizableCanvas canvas = new ResizableCanvas();
		// StackPane stackPane = new StackPane();
		// stackPane.getChildren().add(canvas);
		// canvas.widthProperty().bind(stackPane.widthProperty());
		// canvas.heightProperty().bind(stackPane.heightProperty());

		final ContextMenu contextMenu = new ContextMenu();

		// Menu for the context menu
		MenuItem ctGroup = new MenuItem("Group");
		ctGroup.setAccelerator(KeyCombination.keyCombination("shortcut+G"));

		MenuItem ctDeGroup = new MenuItem("DeGroup");
		ctDeGroup.setAccelerator(KeyCombination.keyCombination("shortcut+D"));

		// Tooltip.install(ctGroup, new Tooltip("Groups the selected shapes"));
		MenuItem ctCut = new MenuItem("Cut");
		ctCut.setAccelerator(KeyCombination.keyCombination("shortcut+X"));
		
		MenuItem ctCopy = new MenuItem("Copy");
		ctCopy.setAccelerator(KeyCombination.keyCombination("shortcut+C"));
		
		MenuItem ctPaste = new MenuItem("Paste");
		ctPaste.setAccelerator(KeyCombination.keyCombination("shortcut+V"));
		
		MenuItem ctRot90deg = new MenuItem("Rotate 90 deg");
		
		MenuItem ctRotMinus90deg = new MenuItem("Rotate -90 deg");
		
		MenuItem ctRot180deg = new MenuItem("Rotate 180 deg");
		
		contextMenu.getItems().addAll(ctGroup, ctDeGroup, ctCut, ctCopy, ctPaste, new SeparatorMenuItem(), ctRot90deg, ctRotMinus90deg, ctRot180deg);

		Group group = new Group();
		group.getChildren().add(canvas);
		AllProperties root = new AllProperties();
		root.setNickName("Root");

		TreeItem<AllProperties> rootItem = new TreeItem<>(root);
		rootItem.setExpanded(true);

		TreeView<AllProperties> treeView = new TreeView<>(rootItem);
		treeView.setPrefHeight(500);
		treeView.setPrefWidth(150);

		Scene scene = new Scene(rootNode, 1000, 600);
		
		
		ToolBar toolbar;
		// Toolbar start
		Button btnSelect = new Button(consts.SELECT, new ImageView("file:images/selection.png"));
		Button btnRotateLeft45Deg = new Button("rotateLeft45", new ImageView("file:images/rotateLeft45.png"));
		Button btnRotateRight45Deg = new Button("rotateRight45", new ImageView("file:images/rotateRight45.png"));
		Button btnLowerLineWidth = new Button("", new ImageView("file:images/minus.png"));
		Button btnHigherLineWidth = new Button("", new ImageView("file:images/plus.png"));
		Button fillBucket = new Button(consts.FILLBUCKET, new ImageView("file:images/fillBucket.png"));
		Button lineColor = new Button(consts.LINECOLOR, new ImageView("file:images/lineColor.png"));
		Slider dashSlider1 = new Slider(1, 50, 1);
		Slider dashSlider2 = new Slider(1, 50, 1);
		Button btnTriangle = new Button("Triangle");
		
		btnRotateLeft45Deg.setAccessibleText("rotateLeft45");
		btnRotateRight45Deg.setAccessibleText("rotateRight45");
		fillBucket.setAccessibleText(consts.FILLBUCKET);
		lineColor.setAccessibleText(consts.LINECOLOR);
		dashSlider1.setAccessibleText("dashSlider1");
		dashSlider2.setAccessibleText("dashSlider2");
		
		btnRotateLeft45Deg.setVisible(false);
		btnRotateRight45Deg.setVisible(false);
		fillBucket.setVisible(false);
		lineColor.setVisible(false);
		dashSlider1.setVisible(false);
		dashSlider2.setVisible(false);
		
		ObservableList<Image> lineToolListImages = FXCollections.observableArrayList(new Image("file:images/line.png"),
				new Image("file:images/dottedLine.png"), new Image("file:images/dashedLine.png"));
		ComboBox<Image> cbLineTool = new ComboBox<Image>(lineToolListImages);
		
		ObservableList<Image> shapeToolImages = FXCollections.observableArrayList(new Image("file:images/rect.png"),
				new Image("file:images/dot.png"), new Image("file:images/ellipse.png"),
				new Image("file:images/polyline.png"), new Image("file:images/polygon.png"));
		ComboBox<Image> cbShapeTool = new ComboBox<Image>(shapeToolImages);
		
		ObservableList<String> lineWidthList = FXCollections.observableArrayList();
		for (int i = 1; i < 20; i++) {
			lineWidthList.add("" + i);
		}
		ComboBox<String> cbLineWidth = new ComboBox<String>(lineWidthList);
		
		toolbar = new ToolBar(btnSelect, new Text("Lines:"), cbLineTool, new Text("Shapes:"), cbShapeTool, btnTriangle,
				new Text("Color:"), colorPicker, new Text("Line width:"), btnLowerLineWidth, cbLineWidth,
				btnHigherLineWidth, new Separator(), btnRotateRight45Deg, btnRotateLeft45Deg, fillBucket, lineColor
				, dashSlider1, dashSlider2);
		// Toolbar end
		
		Label labelXY = new Label();
		Drawing drawing = new Drawing(gc, group, canvas, rootItem, scene, labelXY, toolbar);

		// Toolbar start
		
		btnSelect.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setTool(consts.SELECT);
			}
		});

		btnRotateLeft45Deg.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setRotate(gc, canvas, -45);
			}
		});

		btnRotateRight45Deg.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setRotate(gc, canvas, 45);
			}
		});
		
		btnTriangle.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.setTool("Triangle");
			}
		});

		
		cbLineWidth.setTooltip(new Tooltip("Select the tickness of the lines"));
		cbLineWidth.getSelectionModel().select(0);
		cbLineWidth.setVisible(true);

		cbLineWidth.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setLineWidth(gc, canvas, (double) (cbLineWidth.getSelectionModel().getSelectedIndex() + 1));
				// cbLineTickness.setSelectionModel(cbLineTickness.getSelectionModel().getSelectedItem());
			}
		});

		btnLowerLineWidth.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (!(cbLineWidth.getSelectionModel().getSelectedIndex() == 0)) {
					cbLineWidth.getSelectionModel().select(cbLineWidth.getSelectionModel().getSelectedIndex() - 1);
				}
			}
		});

		btnHigherLineWidth.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (!(cbLineWidth.getSelectionModel().getSelectedIndex() == cbLineWidth.getItems().size())) {
					cbLineWidth.getSelectionModel().select(cbLineWidth.getSelectionModel().getSelectedIndex() + 1);
				}
			}
		});
		
		fillBucket.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setTool(consts.FILLBUCKET);
			}
		});
		
		lineColor.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setTool(consts.LINECOLOR);
			}
		});
		
		dashSlider1.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				//drawing.setDashWidth1(newValue);
				
			}
        });
		dashSlider2.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				drawing.setDashWidth2(newValue);
				
			}
        });

		cbLineTool.setTooltip(new Tooltip("Select different lines"));
		cbLineTool.setCellFactory(new Callback<ListView<Image>, ListCell<Image>>() {
			public ListCell<Image> call(ListView<Image> p) {
				return new ListCell<Image>() {
					private final ImageView imageView = new ImageView();

					public void updateItem(Image item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null || empty) {
							setGraphic(null);
						} else {
							imageView.setImage(item);
							setGraphic(imageView);
							setText("Shape");

						}
					}
				};
			}
		});
		cbLineTool.setCellFactory(c -> new ListImageCell());
		cbLineTool.setButtonCell(new ListImageCell());
		cbLineTool.getSelectionModel().select(0);
		cbLineTool.setVisible(true);

		cbLineTool.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (cbLineTool.getSelectionModel().isSelected(0)) {
					drawing.setTool(consts.LINE);
				} else if (cbLineTool.getSelectionModel().isSelected(1)) {
					drawing.setTool(consts.DOTTEDLINE);
				} else if (cbLineTool.getSelectionModel().isSelected(2)) {
					drawing.setTool(consts.DASHEDLINE);
				}
			}
		});

		cbLineTool.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_PRESSED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						cbLineTool.getSelectionModel().select(0);
						drawing.setTool(consts.LINE);
					}
				});

		cbShapeTool.setTooltip(new Tooltip("Select different shapes"));
		cbShapeTool.setCellFactory(new Callback<ListView<Image>, ListCell<Image>>() {
			public ListCell<Image> call(ListView<Image> p) {
				return new ListCell<Image>() {
					private final ImageView imageView = new ImageView();

					public void updateItem(Image item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null || empty) {
							setGraphic(null);
						} else {
							imageView.setImage(item);
							setGraphic(imageView);
						}
					}
				};
			}
		});
		cbShapeTool.setCellFactory(c -> new ListImageCell());
		cbShapeTool.setButtonCell(new ListImageCell());
		cbShapeTool.getSelectionModel().select(0);
		cbShapeTool.setVisible(true);

		cbShapeTool.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (cbShapeTool.getSelectionModel().isSelected(0)) {
					drawing.setTool(consts.RECTANGLE);
				} else if (cbShapeTool.getSelectionModel().isSelected(1)) {
					drawing.setTool(consts.DOT);
				} else if (cbShapeTool.getSelectionModel().isSelected(2)) {
					drawing.setTool(consts.ELLIPSE);
				} else if (cbShapeTool.getSelectionModel().isSelected(3)) {
					drawing.setTool(consts.POLYLINE);
				} else if (cbShapeTool.getSelectionModel().isSelected(4)) {
					drawing.setTool(consts.POLYGON);
				}
			}
		});

		cbShapeTool.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_PRESSED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						cbShapeTool.getSelectionModel().select(0);
						drawing.setTool(consts.RECTANGLE);
					}
				});

		// ColorPicker
		colorPicker.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				colorPickerText.setFill(colorPicker.getValue());
				drawing.setColor(gc, canvas, colorPicker.getValue());
			}
		});

		// Buttons properties
		btnSelect.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnRotateLeft45Deg.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnRotateRight45Deg.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnLowerLineWidth.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnHigherLineWidth.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		fillBucket.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		lineColor.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		
		btnSelect.setTooltip(new Tooltip("Select a shape"));
		btnRotateLeft45Deg.setTooltip(new Tooltip("Rotate the selected shape 45 degress left"));
		btnRotateRight45Deg.setTooltip(new Tooltip("Rotate the selected shape 45 degress Right"));
		btnLowerLineWidth.setTooltip(new Tooltip("Decrease the line width"));
		btnHigherLineWidth.setTooltip(new Tooltip("Increase the line width"));
		cbLineTool.setTooltip(new Tooltip("Select a line to draw"));
		cbLineWidth.setTooltip(new Tooltip("Select the line width"));
		cbShapeTool.setTooltip(new Tooltip("Select shape to draw"));
		fillBucket.setTooltip(new Tooltip("Fill a shape with the selected color"));
		lineColor.setTooltip(new Tooltip("Change the shape outline color with the selected color"));

		// Event Handlers for all the context menu stuff
		ctGroup.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.setGroup(gc, canvas, group, rootItem);
			}
		});

		ctDeGroup.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.removeGroup(gc, canvas, group, rootItem);
			}
		});
		
		ctCut.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.cut(gc, canvas, group, rootItem);
			}
		});
		
		ctCopy.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.copy(group, rootItem);
			}
		});
		
		ctPaste.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.paste(gc, canvas, group, rootItem);
			}
		});
		
		ctRot90deg.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.setRotate(gc, canvas, 90);
			}
		});
		
		ctRotMinus90deg.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.setRotate(gc, canvas, -90);
			}
		});
		
		ctRot180deg.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				drawing.setRotate(gc, canvas, 180);
			}
		});

		// Menu event Handles
		newFile.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.deleteAllShapes(gc, canvas, group, rootItem);
			}
		});
		save.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.save(group);
			}
		});
		open.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.open(group, rootItem);
			}
		});
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {

			}
		});
		undo.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Platform.exit();
			}
		});

		redo.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {

			}
		});

		cut.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.cut(gc, canvas, group, rootItem);
			}
		});

		copy.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.copy(group, rootItem);
			}
		});

		paste.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.paste(gc, canvas, group, rootItem);
			}
		});

		del.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.deleteSelection(gc, canvas, group, rootItem);
			}
		});

		selectAll.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.selectAll(gc, group, canvas);
			}
		});

		deselectAll.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.deselectAll(gc, canvas);
			}
		});
		
		rotate.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				while (true) {
					try {
						Optional<String> rotateResult = tIDialogRotateAngle.showAndWait();
						if (rotateResult.isPresent()) {
							double rotationalAngle = Double.parseDouble(rotateResult.get());
							if (rotationalAngle < -360) {
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Error, invalid input");
								alert.setContentText("Please write a number higher and -360!");
								alert.showAndWait();
							} else if (rotationalAngle > 360) {
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Error, invalid input");
								alert.setContentText("Please write a number lower than 360!");
								alert.showAndWait();
							} else {
								drawing.setRotate(gc, canvas, rotationalAngle);
								break;
							}
						} else {
							drawing.setRotate(gc, canvas, 0);
							break;
						}
					} catch (Exception e2) {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error, invalid input");
						alert.setContentText("Please write a number!");
						alert.showAndWait();
					}
				}
			}
		});

		scale.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				while (true) {
					try {
						Optional<String> scaleResult = tIDialogScaleSize.showAndWait();
						if (scaleResult.isPresent()) {
							double scale = Double.parseDouble(scaleResult.get());
							if (scale < -100) {
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Error, invalid input");
								alert.setContentText("Please write a number higher and -100%!");
								alert.showAndWait();
							} else if (scale > 100) {
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Error, invalid input");
								alert.setContentText("Please write a number lower than 100%!");
								alert.showAndWait();
							} else {
								drawing.setScale(gc, canvas, scale);
								break;
							}
						} else {
							drawing.setScale(gc, canvas, 0);
							break;
						}
					} catch (Exception e2) {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error, invalid input");
						alert.setContentText("Please write a number!");
						alert.showAndWait();
					}
				}
			}
		});
		
		nintyDegClockWise.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setRotate(gc, canvas, 90);
			}
		});
		
		nintyDegCounterClockWise.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setRotate(gc, canvas, -90);
			}
		});
		
		oneEightyDegClockWise.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				drawing.setRotate(gc, canvas, 180);
			}
		});
		

		// End menu handles

		scene.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED,
				new EventHandler<javafx.scene.input.MouseEvent>() {
					public void handle(javafx.scene.input.MouseEvent e) {
						if (e.getButton() == MouseButton.SECONDARY) {
							contextMenu.show(canvas, e.getScreenX(), e.getScreenY());
						}
					}
				});

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.CONTROL))
					drawing.setControl(true);
			}
		});

		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent e) {
				switch (e.getCode()) {
				case CONTROL:
					drawing.controlPressed();
					break;
				case DELETE:
					drawing.deleteSelection(gc, canvas, group, rootItem);
					break;
				case ENTER:
					drawing.enterPressed(gc, canvas, group, rootItem);
					break;
				default:
					break;
				}
			}
		});

		treeView.setEditable(true);
		treeView.setCellFactory(new Callback<TreeView<AllProperties>, TreeCell<AllProperties>>() {
			public TreeCell<AllProperties> call(TreeView<AllProperties> param) {
				return new TreeCell<AllProperties>() {
					private TextField textField;

					@Override
					public void startEdit() {
						super.startEdit();

						if (textField == null) {
							createTextField();
						}
						setText(null);
						setGraphic(textField);
						textField.selectAll();
					}

					@Override
					public void cancelEdit() {
						super.cancelEdit();
						setText(getItem().getNickName());
						setGraphic(getTreeItem().getGraphic());
					}

					@Override
					public void updateItem(AllProperties item, boolean empty) {
						super.updateItem(item, empty);

						if (empty || item == null) {
							setText(null);
							setGraphic(null);
						} else {
							selectTheNode();
							if (isEditing()) {
								if (textField != null) {
									textField.setText(getString());
								}
								setText(null);
								setGraphic(textField);
							} else {
								setText(getString());
								setGraphic(getTreeItem().getGraphic());
							}
						}
					}

					private void createTextField() {
						textField = new TextField(getString());
						textField.setOnKeyReleased(new EventHandler<KeyEvent>() {

							@Override
							public void handle(KeyEvent t) {
								if (t.getCode() == KeyCode.ENTER) {
									if (getString().equals("Root")) {

									} else {
										getItem().setNickName(textField.getText());
										commitEdit(getItem());
										drawing.getNickName();
									}
								} else if (t.getCode() == KeyCode.ESCAPE) {
									cancelEdit();
								}
							}
						});
					}

					private void selectTheNode() {
						this.setOnMouseReleased(new EventHandler<MouseEvent>() {

							@Override
							public void handle(MouseEvent event) {
								if (!getString().equals("Root")) {
									System.out.println("selected Item: " + getString());
								}
								// drawing.selectTreeViewItem(getItem().getId());
							}

						});

					}

					private String getString() {
						return getItem() == null ? "" : getItem().getNickName();
					}
				};
			}
		});

		VBox vContainer = new VBox();
		HBox hContainer = new HBox();
		hContainer.getChildren().addAll(treeView, group);
		vContainer.getChildren().addAll(menubar, toolbar, hContainer);
		rootNode.setTop(vContainer);
		rootNode.setBottom(labelXY);

		stage.setTitle("Drawing Software");
		stage.setResizable(true);
		stage.sizeToScene();
		stage.setScene(scene);
		stage.show();
		rootNode.requestFocus();
	}

	public class ListImageCell extends ListCell<Image> {
		public void updateItem(Image item, boolean empty) {
			super.updateItem(item, empty);
			ImageView imageView = new ImageView(item);
			if (item == null || empty) {
				setGraphic(null);
			} else {
				setGraphic(imageView);
			}
		}
	}

}
